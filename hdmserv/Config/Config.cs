﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace myhisdservice
{
    public class Config
    {

        public static String REST_HOST = "myhisd.hudsonisd.org";
        public static int REST_PORT = 80;

        private static String CLIENT_ROOT_PATH = "";
        public static String getRootPath()
        {
            if (CLIENT_ROOT_PATH == "")
            {
                CLIENT_ROOT_PATH = Path.GetDirectoryName(Application.ExecutablePath);
            }
            return CLIENT_ROOT_PATH;
        }
        public static String getCurrentVersion()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(asm.Location);
            return "win32-client-" + fvi.ProductVersion;
        }
    }
}
