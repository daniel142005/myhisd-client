﻿using System;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace myhisdservice
{
    static class Program
    {
        /// <summary>
        /// Main entry point of the application. If the command line argument "/update" is passed in,
        /// the auto update process is initiated. Otherwise the service is started. (myHISDService)
        /// </summary>
        static void Main(String[] args)
        {
            if (args.Length == 1 && args[0] == "/update")
            {
                try
                {
                    AutoUpdate.installUpdate();
                }
                catch (Exception ex)
                {
                    Log.WriteError("Program:Main", ex.Message);
                }
                return;
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                { 
                    new myHISD() 
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
