﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 * This class is a base class for the TaskService. Any class that runs within the TaskService will inherit the BaseTask.
 * What this class is responsible for:
 *  - Being able to specify if the task runs at a set time or if it runs in its own thread
 *  - Managing the last time the task was run as well as when it should run again.
 */
namespace myhisdservice.Tasks
{
    public enum TaskPriority
    {
        CRITICAL,
        HIGH,
        NORMAL,
        LOW
    }
    public enum TaskMode
    {
        THREADED,
        SCHEDULED,
        INTERVAL
    }
    public abstract class BaseTask
    {
        protected String _name = null; // The name of the task
        protected double _nextRun; // The unix timestamp of when the task should be run again.
        protected double _lastRun; // The unix timestamp of the last time the task was ran.

        public TaskPriority Priority = TaskPriority.NORMAL;
        public TaskMode Mode = TaskMode.INTERVAL;


        public abstract void Run(); // The method called when it's time for the task to run. 

        public abstract bool IsReady(); // Determines if the class is ready to run. TODO: Shouldn't be abstract

        public String GetName()
        {
            return _name;
        }
    }
}
