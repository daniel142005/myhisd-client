﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using myHISDDataAPI.Logger;

namespace myhisdservice
{
    public static class Log
    {
        private static ServerLogAPI logAPI = new ServerLogAPI(Config.REST_HOST, Config.REST_PORT);

        public static void Write(String cs, String data, int LogLevel)
        {
            try
            {
                ServerLogEntry entry = new ServerLogEntry();
                entry.message = cs + " => " + data;
                entry.type = "myHISDClient";
                TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1));
                entry.datetime = (int)t.TotalSeconds;
                try
                {
                    entry.currentUser = Tools.getCurrentLoggedOnUser();
                }
                catch
                {
                    entry.currentUser = "";
                }
                logAPI.AddEntry(entry);
            }
            catch (Exception ex)
            {
                data += " -- (ServerLogAPI Error: " + ex.Message + ")";
            }
            EventLog.WriteEntry("myHISD", cs + " => " + data);
        }
        public static void WriteInfo(String cs, String data)
        {
            Write(cs, data, 2);
        }
        public static void WriteError(String cs, String data)
        {
            Write(cs, data, 3);
        }
        public static void WriteDebug(String cs, String data)
        {
            Write(cs, data, 1);
        }

    }
}
