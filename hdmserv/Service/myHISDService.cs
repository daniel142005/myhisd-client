﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using myhisdservice.Inventory;
using myhisdservice.AutoUpdater;
using myHISDDataAPI.Inventory.Hardware;
using myHISDDataAPI.AutoUpdate;
/*
 * Service class responsible for handling service threads.
 * Methods:
 * Start - Actions to perform when the service is started. Starts the TaskService
 * Stop - Actions to perform when the service is stopped.
 */
namespace myhisdservice
{
    class myHISDService
    {
        public const String LOG_TAG = "myHISDService";
        public static Tasks.TaskService service;
        public void Start()
        {
            Log.WriteDebug(LOG_TAG, "Starting...");

            try {
                service = new Tasks.TaskService();
            }
            catch (Exception ex)
            {
                Log.WriteError(LOG_TAG + ":Start", "Failed to start TaskService" + ex.Message);
                Stop();
                return;
            }
                        
            // Init APIs
            
            service.AddTask(new AutoUpdateTask(600));

            service.AddTask(new InventoryTask(600));

            service.Start();


            try
            {
                Thread t = new Thread(_doCleanup);
                t.Start();
            }
            catch (Exception ex)
            {
                Log.WriteError(LOG_TAG, "Error cleaning temporary files. -- " + ex.Message);
            }

            Log.WriteDebug(LOG_TAG, "Service Started.");
        }
        private void _doCleanup()
        {
            Thread.Sleep(TimeSpan.FromSeconds(30));
            AutoUpdate.cleanTemporaryFiles();
        }
        public void Stop()
        {
            service.Abort();
            Log.WriteDebug(LOG_TAG, "Service Stopped.");
        }
    }
}
