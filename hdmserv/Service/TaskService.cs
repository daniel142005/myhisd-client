﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
/*
 *  Responsible for running individual tasks. A task should be able to run at a scheduled interval (in the Main Thread) or in it's own thread.
 *  This class is responsible for handling those threads. If a task has an error, the error should be logged.
 *  
 *  
 *  Methods:
 *  Start - Starts the task service
 *  Stop - Safely stops the task service, sending the same command to every managed thread. Automatically aborts after 5 seconds.
 *  Abort - Forcefully stops the task service and all it's managed threads.
 */
namespace myhisdservice.Tasks
{
    class TaskService
    {
        private List<BaseTask> tasks = new List<BaseTask>();
        private bool isRunning = true;
        private Thread _thread;
        private long _startupBackoffSeconds = 2;
        public void AddTask(BaseTask task)
        {
            try
            {
                tasks.Add(task);
            }
            catch (Exception ex)
            {
                Log.WriteError("TaskService:AddTask", ex.Message);
            }
        }
        public void Start()
        {
            try
            {
                _thread = new Thread(Run);
                _thread.Start();
            }
            catch (Exception ex)
            {
                Log.WriteError("TaskService:Start", ex.Message + " --- Retrying...");
                DelayedStart();
            }
        }
        private void DelayedStart()
        {
            try
            {
                if (_startupBackoffSeconds <= 1800)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(_startupBackoffSeconds));
                    _startupBackoffSeconds = _startupBackoffSeconds ^ 2;
                    Start();
                }
            }
            catch (Exception ex)
            {
                Log.WriteError("TaskService:DelayedStart", ex.Message);
            }
        }
        public void Run()
        {
            while (isRunning)
            {
                foreach (BaseTask task in tasks) {
                    try
                    {
                        if (task.IsReady())
                        {
                            task.Run();
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.WriteError("TaskService:Run", task.GetName() + " -- " + ex.Message);
                    }
                }
                Thread.Sleep(5000);
            }
        }
        public void Stop()
        {
            isRunning = false;
        }
        public void Abort()
        {
            try
            {
                if (_thread != null && _thread.IsAlive == true)
                {
                    _thread.Abort();
                }
            }
            catch { }
        }
    }
}
