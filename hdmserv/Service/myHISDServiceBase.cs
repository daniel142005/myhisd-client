﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace myhisdservice
{
    public partial class myHISD : ServiceBase
    {
        private myHISDService serv;
        public myHISD()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            serv = new myHISDService();
            serv.Start();
        }

        protected override void OnStop()
        {
            serv.Stop();
        }
    }
}
