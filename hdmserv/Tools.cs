﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
//using TaskScheduler;

namespace myhisdservice
{
    public static class Tools
    {
        public static String COMPUTER_NAME = System.Environment.MachineName;
        
        public static byte[] getBytesFromString(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string getStringFromBytes(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
        public static byte[] getBytesFromInt(int o)
        {
            return System.BitConverter.GetBytes(o);
        }
        public static String getHDMVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
        public static double GetUnixTimestamp()
        {
            return GetUnixTimestamp(DateTime.UtcNow);
        }
        public static double GetUnixTimestamp(DateTime value)
        {
            TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());

            return Math.Round((double)span.TotalSeconds, 0);
        }
        public static DateTime getDateTimeFromUnixTimestamp(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static String getCurrentLoggedOnUser()
        {
            return Environment.UserName;
        }

        /*
        public static void StartHDMClient()
        {
            //Get a ScheduledTasks object for the local computer.
            ScheduledTasks st = null;
            Task t = null;
            try
            {
                st = new ScheduledTasks();

                // Create a task
                try
                {
                    t = st.CreateTask("HDMC");
                }
                catch (ArgumentException)
                {
                    try
                    {
                        t = st.OpenTask("HDMC");
                        if (t.Status == TaskStatus.NoMoreRuns || t.Status == TaskStatus.NoTriggerTime || t.Status == TaskStatus.NotScheduled)
                        {
                            t.Close();
                            t = null;
                            st.DeleteTask("HDMC");
                        }
                        else
                        {
                            t.Close();
                            t = null;
                        }
                    }
                    catch { }
                    finally
                    {
                        if (st != null)
                        {
                            st.Dispose();
                        }
                    }
                    return;
                }

                // Fill in the program info
                t.ApplicationName = @"T:\hdmclient.exe";
                t.Parameters = "-startmonitor";
                t.Comment = "";

                t.Flags = TaskFlags.RunOnlyIfLoggedOn | TaskFlags.DeleteWhenDone;

                t.SetAccountInformation(System.Environment.MachineName + @"\User", (string)null);

                RunOnceTrigger trig = new RunOnceTrigger(DateTime.Now.AddMinutes(1));
                t.Triggers.Add(trig);
                t.Save();
            }
            catch (Exception ex)
            {
                Log.WriteError("Tools", "StartHDMClient: " + ex.Message);
            }
            finally
            {
                try
                {
                    if (t != null)
                        t.Close();
                }
                catch { }
                try
                {
                    if (st != null)
                        st.Dispose();
                }
                catch { }
            }
        }
         */
    }
}
