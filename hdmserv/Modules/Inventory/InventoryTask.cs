﻿using myHISDDataAPI.Inventory.Hardware;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;

namespace myhisdservice.Inventory
{
    class InventoryTask : Tasks.BaseTask
    {
        public const String LOG_TAG = "InventoryTask";
        InventoryDataCollector inventory = new InventoryDataCollector();
        private int _interval;
        private static HardwareAPI hardwareAPI = new HardwareAPI(Config.REST_HOST, Config.REST_PORT);
        private HardwareEntry entry = null;
        private bool _init = false;
        private bool _initFull = false;
        public InventoryTask(int p)
        {
            _name = "InventoryTask";
            _lastRun = 0;

            this.Priority = Tasks.TaskPriority.NORMAL;
            this.Mode = Tasks.TaskMode.INTERVAL;

            this._interval = p;
            _nextRun = Tools.GetUnixTimestamp();
        }

        public override void Run()
        {
            if (_init == false)
            {
                // Checks to see if the device exists, creating it if it doesn't. Only Occurrs once per startup.
                this.SubmitQuickInventory();
                _init = true;
            }
            else
            {
                if (_initFull == false)
                {
                    // Updates the device with full details. Only occurrs once per startup.
                    this.SubmitFullInventory();
                    _initFull = true;
                }
                else
                {
                    // Sends status updates to notify of the following changes:
                    //      - Device Login/Logout/Shutdown/Restart
                    //      - Current user
                    //      - Device Suspend/Resume
                    //      - Device Idle/Awake (Online but no one has used the device for 2 minutes)
                    this.SubmitStatusUpdate();
                }
            }
            _nextRun = Tools.GetUnixTimestamp() + _interval;
        }

        public override bool IsReady()
        {
            if (Tools.GetUnixTimestamp() >= _nextRun)
                return true;
            else
                return false;
        }
        public bool CheckExists()
        {
            bool found = false;
            try
            {
                entry = hardwareAPI.Get(Tools.COMPUTER_NAME);
                if (entry != null && entry.id != null)
                {
                    found = true;
                }
            }
            catch (Exception ex)
            {
                Log.WriteDebug(LOG_TAG, "HardwareAPI:Get - " + ex.Message);
            }
            return found;
        }
        public void SubmitStatusUpdate()
        {
            // TODO: For now, just resend an update
            hardwareAPI.UpdateEntry(Tools.COMPUTER_NAME, inventory.getHardwareStatusEntry());
        }
        public void SubmitFullInventory()
        {
            // Try to see if the device exists in the inventory
            if (CheckExists())
            {
                inventory.buildAll();
                hardwareAPI.UpdateEntry(Tools.COMPUTER_NAME, inventory.getHardwareEntry());
            }
            else
            {
                inventory.buildAll();
                hardwareAPI.AddEntry(Tools.COMPUTER_NAME, inventory.getHardwareEntry());
            }
        }
        public void SubmitQuickInventory()
        {
            // Try to see if the device exists in the inventory
            if (CheckExists())
            {
                inventory.buildQuick();
                hardwareAPI.UpdateEntry(Tools.COMPUTER_NAME, inventory.getHardwareEntry());
            }
            else
            {
                inventory.buildQuick();
                hardwareAPI.AddEntry(Tools.COMPUTER_NAME, inventory.getHardwareEntry());
            }
        }

    }
}
