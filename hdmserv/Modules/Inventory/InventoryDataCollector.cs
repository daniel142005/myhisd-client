﻿using Microsoft.Win32;
using myHISDDataAPI.Inventory.Hardware;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace myhisdservice.Inventory
{
    class InventoryDataCollector
    {
        public List<Dictionary<string, string>> memoryInfo = new List<Dictionary<string, string>>();
        public List<Dictionary<string, string>> cpuInfo = new List<Dictionary<string, string>>();
        public List<Dictionary<string, string>> printerInfo = new List<Dictionary<string, string>>();
        public List<Dictionary<string, string>> soundInfo = new List<Dictionary<string, string>>();
        public List<Dictionary<string, string>> videoInfo = new List<Dictionary<string, string>>();
        public List<Dictionary<string, string>> diskInfo = new List<Dictionary<string, string>>(); // Hardware
        public List<Dictionary<string, string>> driveInfo = new List<Dictionary<string, string>>(); // Drives (C:, D:, E:, etc.)
        public List<Dictionary<string, string>> partitionInfo = new List<Dictionary<string, string>>();

        public List<Dictionary<string, string>> softwareInfo = new List<Dictionary<string, string>>(); // Incomplete, needs to check registry too

        private HardwareEntry entry = new HardwareEntry();
        

        public void buildQuick()
        {
            buildMemoryInfo();
            buildCpuInfo();
            buildPrinterInfo();
            buildDriveInfo();
        }
        public void buildAll()
        {
            buildMemoryInfo();
            buildCpuInfo();
            buildPrinterInfo();
            buildSoundInfo();
            buildVideoInfo();
            buildDiskInfo();
            buildPartitionInfo();
            buildDriveInfo();
            //buildSoftwareInfo();
        }
        public void buildMemoryInfo()
        {
            memoryInfo = queryWMI("Win32_PhysicalMemory");
        }
        public void buildCpuInfo()
        {
            cpuInfo = queryWMI("Win32_Processor");
        }
        public void buildPrinterInfo()
        {
            printerInfo = queryWMI("Win32_Printer");
        }
        public void buildSoundInfo()
        {
            soundInfo = queryWMI("Win32_SoundDevice");
        }
        public void buildVideoInfo()
        {
            videoInfo = queryWMI("Win32_VideoController");
        }

        public void buildDiskInfo()
        {
            diskInfo = queryWMI("Win32_DiskDrive");
        }

        public void buildPartitionInfo()
        {
            partitionInfo = queryWMI("Win32_DiskPartition");
        }

        public void buildDriveInfo()
        {
            driveInfo = queryWMI("Win32_LogicalDisk");
        }

        public void buildSoftwareInfo()
        {
            softwareInfo = queryWMI("Win32_Product");
        }

        public String toJSON()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        private List<Dictionary<string, string>> queryWMI(string t)
        {
            List<Dictionary<string, string>> r = new List<Dictionary<string, string>>();
            try
            {
                ObjectQuery objectQuery = new ObjectQuery("select * from " + t);
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(objectQuery);
                ManagementObjectCollection vals = searcher.Get();

                //The enumerator of the collection
                ManagementObjectCollection.ManagementObjectEnumerator oe = vals.GetEnumerator();

                while (oe.MoveNext())
                {
                    Dictionary<string, string> info = new Dictionary<string, string>();
                    foreach (PropertyData prop in oe.Current.Properties)
                    {
                        if (prop.Value != null)
                        {
                            info.Add(prop.Name, prop.Value.ToString());
                        }
                    }
                    r.Add(info);
                }
            }
            catch (Exception ex)
            {
                Log.WriteError("InventoryDataCollector:queryWMI", ex.Message);
            }
            return r;
        }

        public HardwareEntry getHardwareEntry()
        {
            entry.id = Tools.COMPUTER_NAME;
            entry.uid = this.getUniqueDeviceId();
            entry.name = Tools.COMPUTER_NAME;
            // IP & Mac Address
            buildIPMacAddresses();
            entry.memoryTotal = GetTotalMemory();
            entry.cpuCores = Environment.ProcessorCount;
            entry.cpuSpeed = getCpuSpeed();
            entry.clientVersion = Config.getCurrentVersion();
            entry.dotNetVersion = "4.0";
            entry.loginState = (int)HardwareLoginState.OFFLINE;
            entry.currentUser = getCurrentUsername();
            entry.osVersion = Environment.OSVersion.ToString();
            entry.type = (int) HardwareType.DESKTOP; // 0 Desktop | 1 laptop | 4 netbook
            entry.mode = (int) HardwareMode.WIRED;
            entry.idleState = (int)HardwareIdleState.ACTIVE;
            entry.description = "Last Updated: " + Tools.GetUnixTimestamp();
            return entry;
        }
        public HardwareEntry getHardwareStatusEntry()
        {
            HardwareEntry entry = new HardwareEntry();
            entry.id = Tools.COMPUTER_NAME;
            entry.uid = this.getUniqueDeviceId();
            entry.name = Tools.COMPUTER_NAME;
            // IP & Mac Address
            buildIPMacAddresses();
            entry.loginState = (int)HardwareLoginState.OFFLINE;
            entry.currentUser = getCurrentUsername();
            entry.idleState = (int)HardwareIdleState.ACTIVE;
            entry.description = "Last Updated: " + Tools.GetUnixTimestamp();
            return entry;
        }
        public enum UIDType
        {
            CPU = 0,
            PRIMARY_HDD = 1,
            MOTHERBOARD = 2,
            NETWORK_CARD = 3
        }
        public String getUniqueDeviceId()
        {
            List<String> uid = new List<String>();

            // Example... TODO!
            uid.Add(getUIDPart(UIDType.CPU, ""));
            uid.Add(getUIDPart(UIDType.PRIMARY_HDD, ""));
            uid.Add(getUIDPart(UIDType.MOTHERBOARD, ""));
            uid.Add(getUIDPart(UIDType.NETWORK_CARD, ""));

            /*
             Unique Device ID (UDID) 
                The UDID is a complex multipart ID separated with hyphens. Each part is used to uniquely identify a device. Each part will contain a device code which will be a single digit number prefixing each part. In addition, each part will contain an MD5 hash of the serial number for that device. At least 3 parts must be present in the UDID, however only 2 may be needed for verification. The parts used may vary from system to system, but will always be unique to that device.

                Device Codes:
                
                CPU 0
                Primary HDD 1
                Motherboard 2
                Network Card 3

                Note: Not all device codes may be present because not every device offers a serial number. 
             * */
            return String.Join("-", uid); // TODO
        }
        private String getUIDPart(UIDType type, String id)
        {
            return ((int)type) + id;
        }
        private String getCurrentUsername()
        {
            string username = "";
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT UserName FROM Win32_ComputerSystem");
                ManagementObjectCollection collection = searcher.Get();
                username = (string)collection.Cast<ManagementBaseObject>().First()["UserName"];
                entry.loginState = (int)HardwareLoginState.LOGGED_IN;
                if (username == null || username == "")
                {
                    entry.loginState = (int)HardwareLoginState.LOGIN_SCREEN;
                }
            }
            catch (Exception ex)
            {
                Log.WriteError("InventoryDataCollector:getCurrentUsername", ex.Message);
            }
            return username;
        }
        private static int GetTotalMemory()
        {
            try
            {
                string Query = "SELECT MaxCapacity FROM Win32_PhysicalMemoryArray";
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(Query);
                double totalSize = 0;
                foreach (ManagementObject WniPART in searcher.Get())
                {
                    double size = (double)Convert.ToUInt32(WniPART.Properties["MaxCapacity"].Value) / 1024;
                    size = Math.Round(size / 1024) * 1024;
                    totalSize += size;
                }
                // Very hackish... needs fix TODO:
                return int.Parse(totalSize.ToString());
            }
            catch (Exception ex)
            {
                Log.WriteError("InventoryDataCollector:GetTotalMemory", ex.Message);
                return -1;
            }
        }
        public static int getCpuSpeed()
        {
            try
            {
                return (int)Registry.GetValue(@"HKEY_LOCAL_MACHINE\HARDWARE\DESCRIPTION\System\CentralProcessor\0", "~MHz", 0);
            }
            catch (Exception ex)
            {
                Log.WriteError("InventoryDataCollector:getCpuSpeed", ex.Message);
                return -1;
            }
        }
        public void buildIPMacAddresses()
        {
            try
            {
                String strHostName = string.Empty;
                // Getting Ip address of local machine...
                // First get the host name of local machine.
                strHostName = Dns.GetHostName();
                // Then using host name, get the IP address list..
                IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
                IPAddress[] addr = ipEntry.AddressList;

                List<String> ips = new List<String>();
                for (int i = 0; i < addr.Length; i++)
                {
                    String ip = addr[i].ToString();
                    if (ip.StartsWith("192.") || ip.StartsWith("10."))
                    {
                        ips.Add(addr[i].ToString());
                    }
                }
                entry.ipAddress = string.Join(",", ips);
                entry.macAddress = NetworkInterface.GetAllNetworkInterfaces().Where(nic => nic.OperationalStatus == OperationalStatus.Up).Select(nic => nic.GetPhysicalAddress().ToString()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.WriteError("InventoryDataCollector:buildIPMacAddress", ex.Message);
                entry.ipAddress = "";
                entry.macAddress = "";
            }
        }
    }
}
