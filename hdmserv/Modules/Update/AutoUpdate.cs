﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;
using System.Web;
using RestSharp;
using System.ServiceProcess;
using Ionic.Zip;

namespace myhisdservice
{
    class AutoUpdate
    {
        
        // Create a copy of the service executable named "tmpupdate.exe", hide it, and run "tmpupdate.exe /update [version]"
        public static void startUpdate()
        {
            try
            {
                cleanTemporaryFiles();
                try
                {
                    File.Copy(Config.getRootPath() + @"\myhisdservice.exe", Config.getRootPath() + @"\tmpupdate.exe");
                }
                finally
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = Config.getRootPath() + @"\tmpupdate.exe";
                    startInfo.Arguments = @"/update";
                    Process.Start(startInfo);
                }
            }
            catch (Exception ex)
            {
                Log.WriteError("AutoUpdate:startUpdate", ex.Message);
            }
        }


        // Copy the new files over the old ones, can't be run in service mode.
        public static void installUpdate()
        {

            String updateFile = Config.getRootPath() + @"\update.dat";
            if (!File.Exists(updateFile))
            {
                Log.WriteError("AutoUpdate:installUpdate", "Could not find update.dat");
                return;
            }

            try
            {
                ServiceController service = new ServiceController("MyHISD");
                TimeSpan timeout = TimeSpan.FromMilliseconds(15000);
                if (service.Status == ServiceControllerStatus.StartPending)
                {
                    service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                }
                if (service.Status != ServiceControllerStatus.Stopped && service.Status != ServiceControllerStatus.StopPending)
                {
                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                }
                Log.WriteDebug("AutoUpdate:installUpdate", "Update File: " + updateFile);
                try
                {

                    if (!File.Exists(updateFile))
                    {
                        Log.WriteInfo("AutoUpdate:installUpdate", "\nNo Updates Found.\n");
                        return;
                    }

                    using (ZipFile zip = ZipFile.Read(updateFile))
                    {
                        foreach (ZipEntry e in zip)
                        {
                            try
                            {
                                e.Extract(Config.getRootPath(), ExtractExistingFileAction.OverwriteSilently);  // overwrite == true  
                            }
                            catch (Exception ex)
                            {
                                Log.WriteError("AutoUpdate:installUpdate", "Could not extract " + e.FileName + " -- " + ex.Message);
                            }
                        }
                    }  

                    File.Delete(updateFile);
                }
                catch (Exception ex)
                {
                    Log.WriteError("AutoUpdate:installUpdate", updateFile + " \n " + ex.Message);
                    return;
                }

                try
                {
                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                }
                catch
                {
                    Log.WriteError("AutoUpdate:installUpdate", "Could not start service");
                    return;
                }
                
            }
            catch (Exception ex)
            {
                Log.WriteError("AutoUpdate:installUpdate", ex.Message);
                return;
            }
            
        }

        // Delete tmpupdate.exe if it exists
        public static void cleanTemporaryFiles()
        {
            if (File.Exists(Config.getRootPath() + @"\tmpupdate.exe"))
            {
                File.Delete(Config.getRootPath() + @"\tmpupdate.exe");
            }
        }


    }
}
