﻿using myHISDDataAPI.AutoUpdate;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace myhisdservice.AutoUpdater
{
    class AutoUpdateTask : Tasks.BaseTask
    {
        private int _interval = 60;
        private UpdateAPI updateAPI = null;
        public AutoUpdateTask(int interval)
        {
            _name = "AutoUpdateTask";
            _lastRun = 0;
            _interval = interval;

            this.Priority = Tasks.TaskPriority.CRITICAL;
            this.Mode = Tasks.TaskMode.INTERVAL;

            _nextRun = Tools.GetUnixTimestamp();
            updateAPI = new UpdateAPI(Config.REST_HOST, Config.REST_PORT); 
        }

        public override void Run()
        {
            AutoUpdate.cleanTemporaryFiles();
            if (updateAPI.hasUpdate("win32", Config.getCurrentVersion()))
            {
                try
                {
                    updateAPI.downloadUpdate(updateAPI.getLatestVersionByPlatform("win32").latestVersion, "win32", Config.getRootPath());
                    updateAPI.Close();
                    AutoUpdate.startUpdate();
                    return;
                }
                catch (Exception ex)
                {
                    Log.WriteError("AutoUpdateTask", ex.Message);
                }
            }
            _nextRun = Tools.GetUnixTimestamp() + _interval;
        }


        public override bool IsReady()
        {
            if (Tools.GetUnixTimestamp() >= _nextRun)
                return true;
            else
                return false;
        }
    }
}
