@echo off
echo Building MyHISD_Common.dll
"C:\Program Files (x86)\Microsoft\ILMerge\ILMerge.exe" /v4 /target:library /out:"M:\Backups\Daniel\Documents\Visual Studio 11\Projects\hdmserv\Shared\MyHISD_Common.dll" "M:\Backups\Daniel\Documents\Visual Studio 11\Projects\hdmserv\Ionic.Zip.Reduced.dll" "M:\Backups\Daniel\Documents\Visual Studio 11\Projects\hdmserv\Newtonsoft.Json.dll" "M:\Backups\Daniel\Documents\Visual Studio 11\Projects\hdmserv\packages\RestSharp.104.1\lib\net4\RestSharp.dll"

echo Building MyHISD.exe
"C:\Program Files (x86)\Microsoft\ILMerge\ILMerge.exe" /v4 /target:winexe /out:"M:\Backups\Daniel\Documents\Visual Studio 11\Projects\hdmserv\hdmserv\bin\Release\MyHISD.exe" "M:\Backups\Daniel\Documents\Visual Studio 11\Projects\hdmserv\hdmserv\bin\Release\MyHISDClient.exe" "M:\Backups\Daniel\Documents\Visual Studio 11\Projects\hdmserv\myHISDDataAPI\bin\Release\myHISDDataAPI.dll"