﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using myhisdservice;
using myHISDDataAPI.Inventory.Hardware;
using System.Threading;
using myHISDDataAPI.AutoUpdate;

namespace MyHISDRestAPITest
{
    class Program
    {
        static void Main(string[] args)
        {
            double score_AutoUpdateAPI = testAutoUpdate();
            double score_hardwareAPI = testHardwareAPI();
            //double score_clientVersion = testClientVersion();
            Console.ReadLine();
        }
        public static double testAutoUpdate()
        {
            int score = 0;
            UpdateAPI api = new UpdateAPI("myhisd.hudsonisd.org", 80);
            myHISDDataAPI.AutoUpdate.VersionInfo latestVersionWin32 = api.getLatestVersionByPlatform("win32");
            myHISDDataAPI.AutoUpdate.VersionInfo latestVersionWin64 = api.getLatestVersionByPlatform("win64");
            LogLn("Latest Win32 Version: " + latestVersionWin32.latestVersion);
            LogLn("Latest Win64 Version: " + latestVersionWin64.latestVersion);
            if (latestVersionWin32.hasUpdate || latestVersionWin64.hasUpdate)
            {
                LogLn("Update Available!");
            }
            //LogLn("Downloading update package...");
            //api.downloadUpdate(latestVersionWin32.latestVersion, "win32", @"M:\Backups\Daniel\Documents\Visual Studio 11\Projects\hdmserv\MyHISDRestAPITest\bin\Release");

            api.Close();
            return (double)score;
        }
        public static double testHardwareAPI()
        {
            int score = 0;
            LogLn("\nStarting Client Version Tests...");
            HardwareAPI api = new HardwareAPI("myhisd.hudsonisd.org", 80);
            LogLn("Creating fake entry HS-RM500");
            HardwareEntry entry = new HardwareEntry();
            entry.id = "HS-RM500";
            entry.name = "HS-RM500";
            entry.loginState = (int)HardwareLoginState.OFFLINE;
            entry.ipAddress = "10.1.9.121";
            entry.macAddress = "00:00:00:00:00";
            entry.memoryTotal = 4000;
            entry.cpuCores = 6;
            entry.cpuSpeed = 4000;
            entry.clientVersion = "win32-v0.1.0.1";
            entry.dotNetVersion = "4.0";
            entry.osVersion = "Windows 7 (64 bit)";
            entry.type = 2;
            entry.description = "";
            api.AddEntry(entry);
            Thread.Sleep(3000);
            try
            {
                entry.cpuCores = 12;
                api.UpdateEntry("HS-RM500", entry);
                LogLn("Update cores to 12... Success!");
            }
            catch (Exception ex)
            {
                LogLn("Update Failed! - " + ex.Message);
            }
            try
            {
                LogLn("Retrieved: " + api.Get(entry.id).id);
            }
            catch (Exception ex)
            {
                LogLn("Retrieved: Failed! - " + ex.Message);
            }
            Thread.Sleep(15000);
            api.RemoveEntry(entry.id, entry);
            try
            {
                String id = null;
                id = api.Get(entry.id).id;
                if (id == null)
                {
                    LogLn("Delete Success");
                }
            }
            catch
            {
                LogLn("Delete Success");
            }
            api.Close();
            return (double)score;
        }
        public static double testClientVersion()
        {
            RestSharp.RestClient api_client = new RestSharp.RestClient("http://myhisd.hudsonisd.org:80");
            LogLn("\nStarting Client Version Tests...");
            int score = 0;
            int max_score = 0;

            RestRequest request = null;
            RestResponse<VersionInfo> response = null;

            LogLn("Retrieving latest client version for win32 platform without version... (Expects OK)".ToUpper());
            max_score += 1;
            try
            {
                request = new RestRequest("/api/0/client/version");
                request.AddParameter("platform", "win32"); // adds to POST or URL querystring based on Method
                response = (RestResponse<VersionInfo>)api_client.Execute<VersionInfo>(request);
                LogLn("Status: " + response.StatusCode);
                LogLn("Type: " + response.ContentType);
                LogLn(response.Content);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    score += 1;
                }
                LogLn("PASSED");
            }
            catch (Exception ex)
            {
                LogLn("FAILED: " + ex.Message);
            }
            LogLn();
            LogLn();

            LogLn("Retrieving latest client version for win32 platform with version 'win32-unknown-1'... (Expects OK)".ToUpper());
            max_score += 1;
            try
            {
                request = new RestRequest("/api/0/client/version");
                request.AddParameter("version", Config.getCurrentVersion());
                LogLn("Adding param 'version' => '" + Config.getCurrentVersion() + "'...");
                request.AddParameter("platform", "win32"); // adds to POST or URL querystring based on Method
                response = (RestResponse<VersionInfo>)api_client.Execute<VersionInfo>(request);
                LogLn("Status: " + response.StatusCode);
                LogLn("Type: " + response.ContentType);
                LogLn(response.Content);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    score += 1;
                }
                LogLn("PASSED");
            }
            catch (Exception ex)
            {
                LogLn("FAILED: " + ex.Message);
            }
            LogLn();
            LogLn();

            LogLn("Retrieving latest client version without platform... (Expects BadRequest)".ToUpper());
            max_score += 1;
            try 
            {
                request = new RestRequest("/api/0/client/version");
                response = (RestResponse<VersionInfo>)api_client.Execute<VersionInfo>(request);
                LogLn("Status: " + response.StatusCode);
                LogLn("Type: " + response.ContentType);
                LogLn(response.Content);
                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    score += 1;
                }
                LogLn("PASSED");
            }
            catch (Exception ex)
            {
                LogLn("FAILED: " + ex.Message);
            }
            LogLn();
            LogLn();

            LogLn("Client Version Tests Complete. Passed " + score + " / " + max_score + " (" + Math.Round(((decimal) (score / max_score)) * 100) + "%)");
            return Math.Round((double)(score / max_score) * 100);
        }

        public static void Log(String text)
        {
            Console.Write(text);
        }
        public static void LogLn(String text)
        {
            Console.WriteLine(text);
        }
        public static void LogLn()
        {
            LogLn("");
        }
    }
}
