﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myHISDDataAPI.Base
{
    public abstract class BaseAPI
    {
        public String domain = "";
        private String apiUsername = "";
        private String apiPassword = "";
        protected String baseURI = "";
        public int port = 80;
        private RestSharp.RestClient api_client = null;
        public String getDomain()
        {
            // TODO: Needs HTTPS
            if (port == 80)
            {
                return "http://" + domain + "";
            }
            else
            {
                return "http://" + domain + ":" + port + "";
            }
        }
        public BaseAPI(String domain)
        {
            this.domain = domain;
        }
        public BaseAPI(String domain, int port)
        {
            this.domain = domain;
            this.port = port;
        }
        protected RestSharp.RestClient GetAPIClient()
        {
            if (api_client == null)
            {
                api_client = new RestSharp.RestClient(this.getDomain());
            }
            return api_client;
        }
        protected String getAPIPath()
        {
            return baseURI;
        }
        public void setAuthentication(String username, String password)
        {
            this.apiPassword = password;
            this.apiUsername = username;
        }
        public void Close()
        {
        }
    }
}
