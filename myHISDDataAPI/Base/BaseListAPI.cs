﻿using RestSharp;
using RestSharp.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace myHISDDataAPI.Base
{
    public abstract class BaseListAPI<T> : BaseAPI where T : BaseEntry, new()
    {
         public BaseListAPI(String domain) : base(domain) 
        {
            Init();
        }
        public BaseListAPI(String domain, int port)
            : base(domain, port)
        {
            Init();
        }
        protected abstract void Init();
        public List<T> GetAll()
        {
            RestRequest request = null;
            RestResponse<List<T>> response = null;
            request = new RestRequest(this.getAPIPath(), Method.GET);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            response = (RestResponse<List<T>>) this.GetAPIClient().Execute<List<T>>(request);
            return response.Data;
        }
        public T Get(String id)
        {
            RestRequest request = new RestRequest(this.getAPIPath() + "/" + id, Method.GET);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            RestResponse<T> response = (RestResponse<T>)this.GetAPIClient().Execute<T>(request);
            return response.Data;
        }
        public bool Exists(String id)
        {
            return false;// NYI
        }
        public T UpdateEntry(String id, T entry)
        {
            Require.Argument("id", entry.id);

            RestRequest request = new RestRequest(this.getAPIPath() + "/" + id, Method.PUT);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(entry);

            return ((RestResponse<T>)this.GetAPIClient().Execute<T>(request)).Data; 
        }
        public T AddEntry(String id, T entry)
        {
            entry.id = id;
            return AddEntry(entry);
        }
        public T AddEntry(T entry)
        {            
            RestRequest request = new RestRequest(this.getAPIPath(), Method.POST);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(entry);

            RestResponse<T> response = ((RestResponse<T>)this.GetAPIClient().Execute<T>(request));
            Console.WriteLine("Status: " + response.StatusCode);
            Console.WriteLine("Type: " + response.ContentType);
            Console.WriteLine(response.Content);
            
            return response.Data;
        }
        public void RemoveEntry(String id, T entry)
        {
            RestRequest request = new RestRequest(this.getAPIPath() + "/" + id, Method.DELETE);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            this.GetAPIClient().Execute<T>(request);
        }
    }
}
