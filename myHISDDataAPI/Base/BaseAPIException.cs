﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myHISDDataAPI.Base
{
    class BaseAPIException : Exception
    {
        public BaseAPIException(string p) : base (p)
        {

        }
    }
}
