﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace myHISDDataAPI.Inventory.Software
{
    class SoftwareAPI : Base.BaseListAPI<SoftwareEntry>
        {

            public SoftwareAPI(String domain)
                : base(domain)
            {
            }
            public SoftwareAPI(String domain, int port)
                : base(domain, port)
            {
            }
            protected override void Init()
            {
                this.baseURI = "/api/0/inventory/software";
            }

        }
    }
