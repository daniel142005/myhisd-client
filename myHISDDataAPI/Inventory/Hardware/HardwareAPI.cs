﻿using RestSharp;
using RestSharp.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myHISDDataAPI.Inventory.Hardware
{
    public class HardwareAPI : Base.BaseListAPI<HardwareEntry>
    {
        
        public HardwareAPI(String domain) : base(domain) 
        {

        }
        public HardwareAPI(String domain, int port)
            : base(domain, port)
        {
        }
        protected override void Init()
        {
            this.baseURI = "/api/0/inventory/hardware";
        }
        
    }
}
