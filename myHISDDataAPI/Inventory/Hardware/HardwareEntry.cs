﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myHISDDataAPI.Inventory.Hardware
{
    public enum HardwareType 
    {
        // 0 - 19 : Connected
        DESKTOP = 1,
        LAPTOP = 2,
        NETBOOK = 3,
        ULTRABOOK = 4,
        TABLET = 5,

        // 20+ : Standalone
        PROJECTOR = 20,
        PRINTER = 21,
        ELMO = 22

    }

    public enum HardwareMode
    {
        STANDALONE = 0,
        WIRED = 1,
        WIRELESS = 2
    }

    public enum HardwareLoginState
    {
        OFFLINE = 0,
        LOGIN_SCREEN = 1,
        LOGGED_IN = 2        
    }

    public enum HardwareIdleState
    {
        ACTIVE = 0,
        IDLE = 1
    }

    public class HardwareEntry : Base.BaseEntry
    {
        public string name { get; set; }
        public string uid { get; set; }
        public int type { get; set; }
        public int mode { get; set; }
        public int idleState { get; set; }
        public int loginState { get; set; }

        public string ipAddress { get; set; }
        public string macAddress { get; set; }
        public string osVersion { get; set; }
        public string dotNetVersion { get; set; }
        public int memoryTotal { get; set; }
        public int cpuSpeed { get; set; }
        public int cpuCores { get; set; }
        public string clientVersion { get; set; }
        public string currentUser { get; set; }
        public string description { get; set; }
    }
}
