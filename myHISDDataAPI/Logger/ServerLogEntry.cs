﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace myHISDDataAPI.Logger
{
    public class ServerLogEntry : Base.BaseEntry
    {
        public string type { get; set; }
        public string message { get; set; }
        public long datetime { get; set; }
        public string currentUser { get; set; }
    }
}
