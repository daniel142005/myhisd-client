﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace myHISDDataAPI.Logger
{
    public class ServerLogAPI : Base.BaseListAPI<ServerLogEntry>
    {
        public ServerLogAPI(String domain) : base(domain) 
        {

        }
        public ServerLogAPI(String domain, int port)
            : base(domain, port)
        {

        }
        protected override void Init()
        {
            this.baseURI = "/api/0/logs";
        }

    }
}
