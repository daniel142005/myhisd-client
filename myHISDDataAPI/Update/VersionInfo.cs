﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myHISDDataAPI.AutoUpdate
{
    public class VersionInfo
    {
        public string latestVersion {get; set;}
        public bool hasUpdate { get; set; }
    }
}
