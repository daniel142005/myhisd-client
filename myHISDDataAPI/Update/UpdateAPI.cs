﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myHISDDataAPI.AutoUpdate
{
    public class UpdateAPI : Base.BaseAPI
    {
        public UpdateAPI(String domain) : base(domain) 
        {
        }
        public UpdateAPI(String domain, int port)
            : base(domain, port)
        {
        }

        /*
         * TODO: Needs constants 
         * WIN32 : win32
         * WIN64 : win64
         * OSX : osx
         * LINUX : linux
         * 
         */
        public VersionInfo getLatestVersionByPlatform(String platform)
        {
            RestRequest request = null;
            RestResponse<VersionInfo> response = null;

            try
            {
                request = new RestRequest("/api/0/client/version");
                request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                request.RequestFormat = DataFormat.Json;
                request.AddParameter("platform", platform); // adds to POST or URL querystring based on Method
                response = (RestResponse<VersionInfo>)this.GetAPIClient().Execute<VersionInfo>(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return response.Data;
                }
                else
                {
                    return null; // Failed TODO:
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Could not retrieve latest version for " + platform + ". " + ex.Message);
            }
        }
        public bool hasUpdate(String platform, String version)
        {
            RestRequest request = null;
            RestResponse<VersionInfo> response = null;

            try
            {
                request = new RestRequest("/api/0/client/version");
                request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                request.RequestFormat = DataFormat.Json;
                request.AddParameter("version", version);
                request.AddParameter("platform", platform);
                response = (RestResponse<VersionInfo>)this.GetAPIClient().Execute<VersionInfo>(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return response.Data.hasUpdate;
                }
                else
                {
                    return false; // Failed TODO:
                }
            }
            catch (Exception ex)
            {
                throw new Exception("hasUpdate for " + platform + " with version " + version + " failed. " + ex.Message);
            }
        }
        

        // Retrieve the update package from the server, zip format
        public void downloadUpdate(String version, String platform, String dir)
        {
            // GET /client/update/download?version=[version]
            // ?version=[version] is optional, if it's not there the latest should be used.

            String updateFile = dir + "/update.dat";
            try
            {
                RestRequest request = new RestRequest("/api/0/client/update");
                request.AddParameter("platform", platform); // adds to POST or URL querystring based on Method
                request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                if (version != null && version != "")
                {
                    request.AddParameter("version", version);
                }
                if (File.Exists(updateFile))
                    File.Delete(updateFile);


                RestResponse response = (RestResponse)this.GetAPIClient().Execute(request);

                File.WriteAllBytes(updateFile, response.RawBytes);
            }
            catch (Exception ex)
            {
                throw new Exception("downloadUpdate for " + platform + " with version " + version + " at '" + dir + "' failed. " + ex.Message);
            }
        }
    }
}
